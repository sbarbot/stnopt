% determine the position of P new GPS stations to optimize
% resolution in the seimogenic zone

clear all


%% Data loading
wdir='../omp/examples/parkfield';

% Poisson's solid
nu=1/4;

% bounds on GPS location
dxlim=[-80 60];
dylim=[-100 80];

stxlim =[-30 30]; numx = 40;
stylim = [-30 30]; numy =40;
%stxlim = [-80 60]; numx = 450;
%stylim = [-100 90]; numy = 450;

stxlim = [-45 25]; numx = 551;
stylim = [-25 50]; numy = 551;

% fault models
fault_models={...
     'pkfd_2x2'
    };

resids={
    [63:72,83:92]
    };

% sample segments to patches
M=0;
for i=1:length(fault_models)
    fname=[wdir '/faults/' fault_models{i} '_patch.flt'];
    [index,~,~,~,~,~,~,~,~]=...
        textread(fname,'%u %f %f %f %f %f %f %f %f','commentstyle','shell');
    M=M+size(index,1); 
end

%% build constraints and smoothing
fm=[];     % fault models
index=[];  % index of individual patches
neighbors=4; % number of neighbors for the Laplacian stencil
for i=1:length(fault_models)
    fname=[wdir '/faults/' fault_models{i} '_patch.flt'];
    [local_index,x1,x2,x3,len,width,strike,dip,rake]=...
        textread(fname,'%u %f %f %f %f %f %f %f %f','commentstyle','shell');
    flt=[x1,x2,x3,len,width,strike,dip,rake];
    N=size(flt,1);
    fm=[fm;flt];
    index=[index; i*1000+local_index];
end
M=size(fm,1);


%% GPS data and Green function
fname=[wdir '/patch_2x2/parkfield_pbo_sub.dat'];
[staNam,gx,gy]=textread(fname,'%s %f %f','commentstyle','shell');
pos=find(gx>=dxlim(1) & gx<=dxlim(2) & gy>=dylim(1) & gy<=dylim(2));
staNam=staNam(pos);gx=gx(pos);gy=gy(pos);

save GPSsub.mat staNam gx gy 

sx = linspace(stxlim(1),stxlim(2),numx);
sy = linspace(stylim(1),stylim(2),numy);
[myx,myy] = meshgrid(sx,sy);
myx = myx(:); myy = myy(:);

%mysta = sprintf('M%03d',[1:length(myx)]');
%mysta = reshape(mysta,[4 length(myx)])';

% number of GPS stations
D1=length(staNam);

% GPS coordinates
yx1=[gy,gx];

% GPS displacement data
VECSIZE1=2;

% build GPS Green functions
fprintf('Construct Green''s functions...\n');
DGF=1;
[G1,REF]=okada_gps(nu,yx1,D1,VECSIZE1,fm,DGF);
CST1=size(REF,2);
[G2,REF2]=okada_gps(nu,[myy,myx],length(myx),VECSIZE1,fm,DGF);


G=[G1, REF];
Q=G*G';
Q=0.5*(Q+Q');

%% Original Resolution
R=G'*pinv(Q)*G;
Rd=diag(R);
rorig=sum(Rd(resids{1}));
Rdorig = Rd(resids{1});

fprintf('Initial resolution (min: %f, mean: %f):\n',min(Rdorig),mean(Rdorig));
fprintf('%d %f \n',[resids{1}',Rdorig]');

fprintf('Progress...\n');
tstart=tic;
%Rdnew = zeros(length(myx),length(resids{1}));
rnew = zeros(length(myx),1);
for k=1:length(myx),
   if (0==mod(k-1,fix(length(myx)/10)))
       fprintf('%3.1f%%\n',(k-1)/length(myx)*100)
   end
   G=[G1,REF;G2(2*k+[-1:0],:),REF2(2*k+[-1:0],:)];
   Q = G*G';
   Q=0.5*(Q+Q');
   R=G'*pinv(Q)*G;
   Rd=diag(R);
%   Rdnew(k,:) = Rd(resids{1});
   rnew(k) = sum(Rd(resids{1}));
end;

%%%%Testing
%  G=[G1,REF;G1(21:22,:),REF(21:22,:)];
%   Q = G*G';
%   Q=0.5*(Q+Q');
%   R=G'*pinv(Q)*G;
%   Rd=diag(R);
%   rtest = sum(Rd(resids{1})) - rorig


rnew = reshape(rnew,[numy numx])-rorig;
%rnew = rnew-rorig;
rmax = max(rnew(:));


toc(tstart);

%% Plot results

xlim = [-45 25];
ylim = [-25 50];

[ii,jj] = find(rnew == max(max(rnew)));


% symmetric stations
strike=-37.6;
ss=sind(strike);
cs=cosd(strike);
xo=2.65;yo=-2.86;
xp=cs*(gx-xo)-ss*(gy-yo);
yp=ss*(gx-xo)+cs*(gy-yo);
gxp=xo-xp*cs+yp*ss;
gyp=yo+xp*ss+yp*cs;


% geographic reference
load ca_faults_dim.dat
load ca_faults_km.dat
load ca_coast_dim.dat
load ca_coast_km.dat

% plot data and forward model
fontsize=8;
figure(1);clf;cla
hold on
[xp,yp,~,~]=transform4patch_general(fm(:,2),fm(:,1),fm(:,3),zeros(M,1),...
    fm(:,4),fm(:,5),fm(:,7),fm(:,6));
imagesc(sx,sy,rnew); caxis([0 1]);
plot_faults(ca_faults_km,ca_faults_dim,xlim,ylim);
plot_faults(ca_coast_km,ca_coast_dim,xlim,ylim);
patch(xp,yp,0*xp,'EdgeColor','k','FaceColor','w');

%scatter(myx,myy,50,rnew,'filled'); caxis([0 1]);
colorbar;
plot(0,0,'kp','MarkerSize',10,'MarkerFaceColor','r')
plot(-12.5514, 15.8870,'kp','MarkerSize',10,'MarkerFaceColor','g')
plot(gx,gy,'k^')
plot(gxp,gyp,'k.')
plot(sx(jj),sy(ii),'ko');
%for k=1:length(staNam)
%    text(gx(k),gy(k),staNam(k),'FontSize',8)
%end
axis equal tight;box on
set(gca,'xlim',xlim,'ylim',ylim)
box on
ylabel('north (km)')
xlabel('distance (km)')
title('tr(Rnew) - tr(Rorig) function of GPS position')

