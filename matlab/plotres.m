% determine the position of P new GPS stations to optimize
% resolution in the seimogenic zone

clear all
numnew = 20;

%% Data loading
wdir='..';

% Poisson's solid
nu=1/4;

% bounds on GPS location
dxlim=[-80 60];
dylim=[-100 80];

% fault models
fault_models={...
%    'parkfield_coseismic_2x2',...
     'pkfd_2x2',...
%    'parkfield_intermediate',...
    };

resids={
%    [63:74,83:94],...
    [83:94],...
    %7:12 ...
    };

load SHS_min.mat

% sample segments to patches
M=0;
for i=1:length(fault_models)
    fname=[wdir '/faults/' fault_models{i} '_patch.flt'];
    [index,~,~,~,~,~,~,~,~]=...
        textread(fname,'%u %f %f %f %f %f %f %f %f','commentstyle','shell');
    M=M+size(index,1); 
end

%% build constraints and smoothing
fm=[];     % fault models
index=[];  % index of individual patches
neighbors=4; % number of neighbors for the Laplacian stencil
for i=1:length(fault_models)
    fname=[wdir '/faults/' fault_models{i} '_patch.flt'];
    [local_index,x1,x2,x3,len,width,strike,dip,rake]=...
        textread(fname,'%u %f %f %f %f %f %f %f %f','commentstyle','shell');
    flt=[x1,x2,x3,len,width,strike,dip,rake];
    N=size(flt,1);
    fm=[fm;flt];
    index=[index; i*1000+local_index];
end
M=size(fm,1);


%% GPS data and Green function
fname=[wdir '/gps/pbo_stations_km.dat'];
[staNam,gx,gy]=textread(fname,'%s %f %f','commentstyle','shell');
pos=find(gx>=dxlim(1) & gx<=dxlim(2) & gy>=dylim(1) & gy<=dylim(2));
staNam=staNam(pos);gx=gx(pos);gy=gy(pos);

% number of GPS stations
D1=length(staNam);

% GPS coordinates
yx1=[gy,gx];

% GPS displacement data
VECSIZE1=2;

% build GPS Green functions
DGF=1;
[G1,REF]=okada_gps(nu,yx1,D1,VECSIZE1,fm,DGF);
CST1=size(REF,2);

sx = myx(res(numnew).inds); sy = myy(res(numnew).inds);
[G2,REF2]=okada_gps(nu,[sy,sx],length(sx),VECSIZE1,fm,DGF);

% The design matrix and data vector with only data constraints are
%
%     G = | G1 REF |;
%
% for the model parameters
%
%         | fault patch (M)    |
%     m = |                    |
%         | reference   (CST1) |
%
G=[G1, REF;G2,REF2];


%% Resolution

R=G'*pinv(G*G')*G;
Rd=diag(R);
r=sum(Rd);
[res(numnew).resmat  Rd(resids{1})]
%% Plot results

xlim=[-80 60];
xlim = [-40 20];
%ylim=[-80 40]; % south of Parkfield
ylim=[-80 90]; % around Parkfield
ylim = [-30 40];
cxlim=[-100 100];
cylim=[-50 100];

load ca_faults_dim.dat
load ca_faults_km.dat
load ca_coast_dim.dat
load ca_coast_km.dat

% 2004 ncedc catalog
fname=[wdir '/seismicity/ncedc.2004.km.xydm'];
[eqx04,eqy04,eqdepth04,eqmag04]=textread(fname,'%f %f %f %f','commentstyle','shell');

% plot data and forward model
fontsize=8;
figure;clf;cla
hold on
[xp,yp,~,~]=transform4patch_general(fm(:,2),fm(:,1),fm(:,3),zeros(M,1),...
    fm(:,4),fm(:,5),fm(:,7),fm(:,6));
patch(xp,yp,0*xp,'EdgeColor','k','FaceColor','w');
plot_faults(ca_faults_km,ca_faults_dim,xlim,ylim);
plot_faults(ca_coast_km,ca_coast_dim,cxlim,cylim);
plot(0,0,'kp','MarkerSize',10,'MarkerFaceColor','r')
plot(-12.5514, 15.8870,'kp','MarkerSize',10,'MarkerFaceColor','g')
plot(gx,gy,'k^')
plot(sx,sy,'ko');
%for k=1:length(staNam)
%    text(gx(k),gy(k),staNam(k),'FontSize',8)
%end
axis equal tight;box on
set(gca,'xlim',xlim,'ylim',ylim)
box on
ylabel('north (km)')
title('GPS position')


%% plot resolution of model parameters
figure;clf;
cla;
hold on
% resolution: Resd(1:M); penalty
[xp,yp,zp,up]=transform4patch_general(fm(:,2),fm(:,1),fm(:,3),Rd(1:M),...
    fm(:,4),min(fm(:,5),35),fm(:,7),fm(:,6));
patch(xp,yp,-zp,up);
colorbar
axis equal tight; colorbar;view([-8,26]);
set(gca,'xlim',xlim,'ylim',ylim,'zlim',[-20 0]);
set(gca,'clim',[0 1])
%title(['svd estimate of resolution ' num2str(M) ...
%    ' params, trace(R)=' sprintf('%2.1f',sum(Rd(1:M))) ...
%    ' r=' num2str(r)])
xlabel('east (km)')
ylabel('north (km)')
camva(4)

figure;clf
plot(orig.rorig+[res.frac]);
title('Min Resolution in seismogenic zone')
xlabel('Number of stations');
ylabel('Resolution')

