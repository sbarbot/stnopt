function [outinds] = refshs(Gorig,Gnew,inds,resinds)

Norig = size(Gorig,1);
Ncol  = size(Gorig,2);
Nnew  = size(Gnew,1)/2;

flag = 0;
count = 1;

while(flag==0)

indsleft = setdiff([1:Nnew],inds);
numleft  = length(indsleft);
numinds  = length(inds);
disp(sprintf('Iteration: %d with %d rows',[count numinds]));

kinds = 2*repmat(inds,2,1)+repmat([-1:0]',1,length(inds));
kinds = kinds(:);
G=[Gorig;Gnew(kinds,:)];
Q=G*G';
Q=0.5*(Q+Q');
R=G'*pinv(Q)*G;
Rd=diag(R);
rorig = sum(Rd(resinds));

res = zeros(numinds,numleft);

for m=1:numinds,
   compinds = setdiff([1:numinds],m);
   compinds = inds(compinds);
   for n=1:numleft,
       newinds = [compinds,indsleft(n)];
       kinds = 2*repmat(newinds,2,1)+repmat([-1:0]',1,length(newinds));
       kinds = kinds(:);

       G = [Gorig;Gnew(kinds,:)];
       Q=G*G';
       Q=0.5*(Q+Q');
       R=G'*pinv(Q)*G;
       Rd=diag(R);
       res(m,n) = sum(Rd(resinds));
   end;
end;

res = res - rorig;
maxv = max(res(:));
if(maxv<= 0)
    flag = 1;
else
    [ii,jj] = find(res == maxv);
    ii = ii(1); jj=jj(1);     %%%%Unique maxima.
    compinds = setdiff([1:numinds],ii);
    inds = sort([inds(compinds),indsleft(jj)]); 
    flag = 0;
end;
count = count+1;
end;

outinds = inds;
