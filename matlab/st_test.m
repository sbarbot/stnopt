% determine the position of P new GPS stations to optimize
% resolution in the seimogenic zone

clear all


%% Data loading
wdir='../omp/examples/parkfield';

% Poisson's solid
nu=1/4;

% bounds on GPS location
dxlim=[-80 60];
dylim=[-100 80];

stxlim =[-30 30]; numx = 40;
stylim = [-30 30]; numy =40;
%stxlim = [-80 60]; numx = 450;
%stylim = [-100 90]; numy = 450;

stxlim = [-30 30]; numx = 60;
stylim = [-30 30]; numy = 60;

% fault models
fault_models={...
    'pkfd_2x2',...
    };

resids={
    [63:72,83:92],...
    };

% sample segments to patches
M=0;
for i=1:length(fault_models)
    fname=[wdir '/faults/' fault_models{i} '_patch.flt'];
    [index,~,~,~,~,~,~,~,~]=...
        textread(fname,'%u %f %f %f %f %f %f %f %f','commentstyle','shell');
    M=M+size(index,1); 
end

%% build constraints and smoothing
fm=[];     % fault models
index=[];  % index of individual patches
neighbors=4; % number of neighbors for the Laplacian stencil
for i=1:length(fault_models)
    fname=[wdir '/faults/' fault_models{i} '_patch.flt'];
    [local_index,x1,x2,x3,len,width,strike,dip,rake]=...
        textread(fname,'%u %f %f %f %f %f %f %f %f','commentstyle','shell');
    flt=[x1,x2,x3,len,width,strike,dip,rake];
    N=size(flt,1);
    fm=[fm;flt];
    index=[index; i*1000+local_index];
end
M=size(fm,1);

%% GPS data and Green function
fname=[wdir '/patch_2x2/parkfield_pbo_sub.dat'];
[staNam,gx,gy]=textread(fname,'%s %f %f','commentstyle','shell');
pos=find(gx>=dxlim(1) & gx<=dxlim(2) & gy>=dylim(1) & gy<=dylim(2));
staNam=staNam(pos);gx=gx(pos);gy=gy(pos);

sx = linspace(stxlim(1),stxlim(2),numx);
sy = linspace(stylim(1),stylim(2),numy);
[myx,myy] = meshgrid(sx,sy);
myx = myx(:); myy = myy(:);

%mysta = sprintf('M%03d',[1:length(myx)]');
%mysta = reshape(mysta,[4 length(myx)])';

% number of GPS stations
D1=length(staNam);

% GPS coordinates
yx1=[gy,gx];

% GPS displacement data
VECSIZE1=2;

% build GPS Green functions
DGF=1;
[G1,REF]=okada_gps(nu,yx1,D1,VECSIZE1,fm,DGF);
CST1=size(REF,2);

G=[G1,REF];
Q=G*G';
Q=0.5*(Q+Q');
%% Original Resolutio
%[V,D] = eig(Q);
%[[1:160]',V(:,end)]
S = pinv(Q);
S = 0.5*(S+S');
R=G'*S*G;
Rd=diag(R);
