% plot resolution matrix

%% Data loading
wdir='../omp/examples/parkfield';

% Poisson's solid
nu=1/4;

% bounds on GPS location
dxlim=[-80 60];
dylim=[-100 80];

% fault models
fault_models={...
     'pkfd_2x2'
    };

% sample segments to patches
M=0;
for i=1:length(fault_models)
    fname=[wdir '/faults/' fault_models{i} '_patch.flt'];
    [index,~,~,~,~,~,~,~,~]=...
        textread(fname,'%u %f %f %f %f %f %f %f %f','commentstyle','shell');
    M=M+size(index,1); 
end

%% build constraints and smoothing
fm=[];     % fault models
index=[];  % index of individual patches
neighbors=4; % number of neighbors for the Laplacian stencil
for i=1:length(fault_models)
    fname=[wdir '/faults/' fault_models{i} '_patch.flt'];
    [local_index,x1,x2,x3,len,width,strike,dip,rake]=...
        textread(fname,'%u %f %f %f %f %f %f %f %f','commentstyle','shell');
    flt=[x1,x2,x3,len,width,strike,dip,rake];
    N=size(flt,1);
    fm=[fm;flt];
    index=[index; i*1000+local_index];
end
M=size(fm,1);


%% GPS data and Green function
fname=[wdir '/patch_2x2/parkfield_pbo_sub.dat'];
[staNam,gx,gy]=textread(fname,'%s %f %f','commentstyle','shell');
pos=find(gx>=dxlim(1) & gx<=dxlim(2) & gy>=dylim(1) & gy<=dylim(2));
staNam=staNam(pos);gx=gx(pos);gy=gy(pos);

% add the new stations
%load piyush.mat;
load SHS_min_bottom.mat;

ADD=3;

% number of GPS stations
D1=length(staNam)+ADD;

% station position
inds=res(ADD).inds;
yx1=[[gy;myy(inds)],[gx;myx(inds)]];


%% Green's function and resolution

% GPS displacement data
VECSIZE1=2;

% build GPS Green functions
fprintf('Construct Green''s functions...\n');
DGF=1;
[G1,REF]=okada_gps(nu,yx1,D1,VECSIZE1,fm,DGF);
CST1=size(REF,2);
[G2,REF2]=okada_gps(nu,[myy,myx],length(myx),VECSIZE1,fm,DGF);


G=[G1, REF];
Q=G*G';
Q=0.5*(Q+Q');
Rd=diag(G'*pinv(Q)*G);

%% plot

xlim = [-45 25];
ylim = [-25 50];

% symmetric stations
strike=-37.6;
ss=sind(strike);
cs=cosd(strike);
xo=2.65;yo=-2.86;
xp=cs*(gx-xo)-ss*(gy-yo);
yp=ss*(gx-xo)+cs*(gy-yo);
gxp=xo-xp*cs+yp*ss;
gyp=yo+xp*ss+yp*cs;

xp=cs*(myx-xo)-ss*(myy-yo);
yp=ss*(myx-xo)+cs*(myy-yo);
myxp=xo-xp*cs+yp*ss;
myyp=yo+xp*ss+yp*cs;

% geographic reference
load ca_faults_dim.dat
load ca_faults_km.dat
load ca_coast_dim.dat
load ca_coast_km.dat

% 2004 ncedc catalog
fname=[wdir '/seismicity/ncedc.2004.all.km.xydm'];
[eqx04,eqy04,eqdepth04,eqmag04]=textread(fname,'%f %f %f %f','commentstyle','shell');

pos=find(eqx04 > xlim(1) & eqx04 < xlim(2) & eqy04 > ylim(1) & eqy04 < ylim(2) & eqdepth04 < 15);
eqx04=eqx04(pos);eqy04=eqy04(pos);eqdepth04=eqdepth04(pos);eqmag04=eqmag04(pos);


% plot data and forward model
f.x=fm(:,2);
f.y=fm(:,1);
f.z=fm(:,3);
f.Rd=Rd(1:M);
f.length=fm(:,4);
f.width=fm(:,5);
f.strike=fm(:,6);
f.dip=fm(:,7);
f.rake=fm(:,8);
[xp,yp,zp,up]=transform4patch_general(f.x,f.y,f.z,f.Rd,f.length,f.width,f.dip,f.strike);

fontsize=8;

figure(1);clf;

subplot(3,1,1);cla
hold on
plot_faults(ca_faults_km,ca_faults_dim,xlim,ylim);
plot_faults(ca_coast_km,ca_coast_dim,xlim,ylim);
patch(xp,yp,zp,'EdgeColor','k','FaceColor','w');

plot(0,0,'kp','MarkerSize',10,'MarkerFaceColor','r')
plot(-12.5514, 15.8870,'kp','MarkerSize',10,'MarkerFaceColor','g')
plot(-66.3175,-9.58303,'kp','MarkerSize',10,'MarkerFaceColor','k')
plot(-4.961,9.583,'k.');
plot(6.645,-10.384,'k.');
plot(gx,gy,'k^')
plot(gxp,gyp,'k.','markersize',6)
plot(myx(inds),myy(inds),'k*');
plot(myxp(inds),myyp(inds),'r*');
plot(eqx04,eqy04,'ko','MarkerSize',1);
%plot(sx(jj),sy(ii),'ko');
%for k=1:length(staNam)
%    text(gx(k),gy(k),staNam(k),'FontSize',8)
%end
axis equal tight;box on
set(gca,'xlim',xlim,'ylim',ylim), box on;
ylabel('north (km)')
xlabel('distance (km)')
title(sprintf('resolution with %d stations',ADD))




subplot(3,1,2);cla;

origx=mean(f.x);
origy=mean(f.y);
origz=mean(f.z);
mstrike=mean(f.strike);
mdip=mean(f.dip);


n=[-cosd(mstrike)*sind(mdip),+sind(mstrike)*sind(mdip),-cosd(mdip)];
d=[+cosd(mstrike)*cosd(mdip),-sind(mstrike)*cosd(mdip),-sind(mdip)];
s=[-sind(mstrike),-cosd(mstrike), 0];

px=reshape((xp(:)-origx)*s(1)+(yp(:)-origy)*s(2)+zp(:)*s(3),4,N);
py=reshape((xp(:)-origx)*d(1)+(yp(:)-origy)*d(2)+zp(:)*d(3),4,N);
pz=reshape((xp(:)-origx)*n(1)+(yp(:)-origy)*n(2)+zp(:)*n(3),4,N);

peqx=(eqx04-origx)*s(1)+(eqy04-origy)*s(2)+eqdepth04*s(3);
peqy=(eqx04-origx)*d(1)+(eqy04-origy)*d(2)+eqdepth04*d(3);
peqz=(eqx04-origx)*n(1)+(eqy04-origy)*n(2)+eqdepth04*n(3);
pos=find(abs(peqz)<4 & peqx < max(px(:)) & peqx > min(px(:)) & peqy > min(py(:)) & peqy < max(py(:)));
plot(peqx(pos),peqy(pos),'ko','MarkerSize',3)

hold on
patch(px,py,zeros(4,N),up);
cmap=rot90(hot)';
    colormap(cmap)
colorbar
set(gca,'clim',[0 1])
xlabel('strike direction');
ylabel('dip dircetion');
title(sprintf('%s (cm/yr)',fault_models{i}));
axis equal tight, box on

subplot(3,1,3);cla
hold on
plot_faults(ca_faults_km,ca_faults_dim,xlim,ylim);
plot_faults(ca_coast_km,ca_coast_dim,xlim,ylim);
%patch(xp,yp,zp,'EdgeColor','k','FaceColor','w');
patch(xp,yp,-zp,up,'EdgeColor','k');

%scatter(myx,myy,50,rnew,'filled'); caxis([0 1]);
%colorbar;
plot3(0,0,-8.5,'kp','MarkerSize',10,'MarkerFaceColor','r')
plot3(-12.5514, 15.8870,-8.5,'kp','MarkerSize',10,'MarkerFaceColor','g')
plot3(-66.3175,-9.58303,-7.5,'kp','MarkerSize',10,'MarkerFaceColor','k')
plot3(-4.961,9.583,0,'k.');
plot3(6.645,-10.384,0,'k.');
plot3(gx,gy,0*gx,'k^')
plot3(gxp,gyp,0*gxp,'k.','markersize',6)
plot3(myx(inds),myy(inds),0*myy(inds),'k+');
plot3(eqx04,eqy04,-eqdepth04,'ko','MarkerSize',2);
%plot(sx(jj),sy(ii),'ko');
%for k=1:length(staNam)
%    text(gx(k),gy(k),staNam(k),'FontSize',8)
%end
axis equal tight;box on
set(gca,'xlim',xlim,'ylim',ylim), box on, view([-18,32]);
ylabel('north (km)')
xlabel('distance (km)')
title(sprintf('resolution with %d stations',ADD))

pos=get(gcf,'Position');
pos(3:4)=[486 1100];
set(gcf,'Position',pos);


%% print the list of new stations

zone=11;
lat0=+35.8150;
lon0=-120.370;
[x0,y0]=utm2ll(lon0,lat0,zone,1);

[lon ,lat ]=utm2ll(x0+myx(inds),y0+myy(inds),zone,2);
[lonp,latp]=utm2ll(x0+myxp(inds),y0+myyp(inds),zone,2);


fprintf('rorig: %f\n',orig.rorig);
fprintf('rnew, min: %f, max: %f, mean: %f\n',min(res(ADD).resmat),max(res(ADD).resmat),mean(res(ADD).resmat));
fprintf('New position for %d additional GPS stations:\n',ADD);
fprintf('# longitude, latitude, longitude'', latitude''\n',ADD);
fprintf('%f %f %f %f\n',[lon,lat,lonp,latp]');



return

%% plot the efficiency curve


minrestop=zeros(1+length(res),1);
minresbot=zeros(1+length(res),1);

for k=1:length(res)
    minrestop(k+1)=min(res(k).fullres(83:92));
    minresbot(k+1)=min(res(k).fullres(63:72));
end

figure
plot(minrestop)