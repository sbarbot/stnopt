% plot resolution for each new stations from the log file
%
% needs to create separate files for each new set of stations
%
% grep "tation" patch_2x2_p/parkfield_pbo_sub.dat.log | awk '{for (k=2;k<=NF;k++){print $k > "patch_2x2_p/"NR"new_station.dat"}}'

%% Data loading
wdir='../omp/examples/parkfield';

% Poisson's solid
nu=1/4;

% bounds on GPS location
dxlim=[-80 60];
dylim=[-100 80];

% fault models
fault_models={...
    'pkfd_2x2'
    };

% sample segments to patches
M=0;
for i=1:length(fault_models)
    fname=[wdir '/faults/' fault_models{i} '_patch.flt'];
    [index,~,~,~,~,~,~,~,~]=...
        textread(fname,'%u %f %f %f %f %f %f %f %f','commentstyle','shell');
    M=M+size(index,1);
end

%% build constraints and smoothing
fm=[];     % fault models
index=[];  % index of individual patches
neighbors=4; % number of neighbors for the Laplacian stencil
for i=1:length(fault_models)
    fname=[wdir '/faults/' fault_models{i} '_patch.flt'];
    [local_index,x1,x2,x3,len,width,strike,dip,rake]=...
        textread(fname,'%u %f %f %f %f %f %f %f %f','commentstyle','shell');
    flt=[x1,x2,x3,len,width,strike,dip,rake];
    N=size(flt,1);
    fm=[fm;flt];
    index=[index; i*1000+local_index];
end
M=size(fm,1);


%% GPS data and Green function
fname=[wdir '/patch_2x2/parkfield_pbo_sub.dat'];
[staNam,gx,gy]=textread(fname,'%s %f %f','commentstyle','shell');
pos=find(gx>=dxlim(1) & gx<=dxlim(2) & gy>=dylim(1) & gy<=dylim(2));
staNam=staNam(pos);gx=gx(pos);gy=gy(pos);


%% Green's function and resolution

% GPS displacement data
VECSIZE1=2;
DGF=1;

% new candidate stations
fname=[wdir '/patch_2x2_p/parkfield-new_cand_stations.km'];
[~,myx,myy]=textread(fname,'%d %f %f','commentstyle','shell');

MAXSTA=40;

minrestop=zeros(1+MAXSTA,1);
minresbot=zeros(1+MAXSTA,1);
meanrestop=zeros(1+MAXSTA,1);
meanresbot=zeros(1+MAXSTA,1);

% number of GPS stations
D1=length(staNam);

% station position
yx1=[gy,gx];

% build GPS Green functions
[G1,REF]=okada_gps(nu,yx1,D1,VECSIZE1,fm,DGF);

G=[G1, REF];
%G=G1;
Q=G*G';
Q=0.5*(Q+Q');
Rd=diag(G'*pinv(Q)*G);

minrestop(1)=min(Rd(83:92));
minresbot(1)=min(Rd(63:72));

meanrestop(1)=mean(Rd(83:92));
meanresbot(1)=mean(Rd(63:72));

resolution=cell(MAXSTA,1);

for k=1:MAXSTA
    fprintf('processing new station %d\n',k);
    
    resolution{k}=struct('Rd',[],'R',[]);
    
    % new candidate stations
    fname=sprintf('%s/patch_2x2_p/%dnew_station.dat',wdir,k);
    [inds]=textread(fname,'%d','commentstyle','shell');
    inds=inds+1;
    
    % number of GPS stations
    D1=length(staNam)+k;
    
    % station position
    yx1=[[gy;myy(inds)],[gx;myx(inds)]];
    
    % build GPS Green functions
    [G1,REF]=okada_gps(nu,yx1,D1,VECSIZE1,fm,DGF);
    
    G=[G1, REF];
    %G=[G1];
    Q=G*G';
    Q=0.5*(Q+Q');
    resolution{k}.R=G'*pinv(Q,2.22e-16*norm(Q)*size(Q,1))*G;
    %resolution{k}.R=G'*pinv(Q)*G;
    resolution{k}.Rd=diag(resolution{k}.R);
    
    minrestop(1+k)=min(resolution{k}.Rd(83:92));
    minresbot(1+k)=min(resolution{k}.Rd(63:72));
    
    meanrestop(1+k)=mean(resolution{k}.Rd(83:92));
    meanresbot(1+k)=mean(resolution{k}.Rd(63:72));
    
end

%% plot

figure(2);clf;
hold on
plot(0:MAXSTA,minrestop)
plot(0:MAXSTA,minresbot)

plot(0:MAXSTA,meanrestop)
plot(0:MAXSTA,meanresbot)

box on
set(gca,'ylim',[0 1]);
xlabel('additional GPS stations')


ADD=20;

figure(3);clf

fname=sprintf('%s/patch_2x2_p/%dnew_station.dat',wdir,ADD);
[inds]=textread(fname,'%d','commentstyle','shell');
inds=inds+1;

% symmetric stations
strike=-37.6;
ss=sind(strike);
cs=cosd(strike);
xo=2.65;yo=-2.86;
xp=cs*(gx-xo)-ss*(gy-yo);
yp=ss*(gx-xo)+cs*(gy-yo);
gxp=xo-xp*cs+yp*ss;
gyp=yo+xp*ss+yp*cs;

xp=cs*(myx-xo)-ss*(myy-yo);
yp=ss*(myx-xo)+cs*(myy-yo);
myxp=xo-xp*cs+yp*ss;
myyp=yo+xp*ss+yp*cs;

% geographic reference
load ca_faults_dim.dat
load ca_faults_km.dat
load ca_coast_dim.dat
load ca_coast_km.dat

f.x=fm(:,2);
f.y=fm(:,1);
f.z=fm(:,3);
f.Rd=resolution{ADD}.Rd(1:M);
f.length=fm(:,4);
f.width=fm(:,5);
f.strike=fm(:,6);
f.dip=fm(:,7);
f.rake=fm(:,8);
[xp,yp,zp,up]=transform4patch_general(f.x,f.y,f.z,f.Rd,f.length,f.width,f.dip,f.strike);

subplot(2,1,1);cla;
hold on
plot_faults(ca_faults_km,ca_faults_dim,xlim,ylim);
plot_faults(ca_coast_km,ca_coast_dim,xlim,ylim);
patch(xp,yp,zp,'EdgeColor','k','FaceColor','w');

plot(0,0,'kp','MarkerSize',10,'MarkerFaceColor','r')
plot(-12.5514, 15.8870,'kp','MarkerSize',10,'MarkerFaceColor','g')
plot(-66.3175,-9.58303,'kp','MarkerSize',10,'MarkerFaceColor','k')
plot(-4.961,9.583,'k.');
plot(6.645,-10.384,'k.');
plot(gx,gy,'k^')
plot(gxp,gyp,'k.','markersize',6)
plot(myx(inds),myy(inds),'k*');
plot(myxp(inds),myyp(inds),'r*');
plot(eqx04,eqy04,'ko','MarkerSize',1);
%plot(sx(jj),sy(ii),'ko');
%for k=1:length(staNam)
%    text(gx(k),gy(k),staNam(k),'FontSize',8)
%end
axis equal tight;box on
set(gca,'xlim',xlim,'ylim',ylim), box on;
ylabel('north (km)')
xlabel('distance (km)')
title(sprintf('resolution with %d stations',ADD))

subplot(2,1,2);cla;

origx=mean(f.x);
origy=mean(f.y);
origz=mean(f.z);
mstrike=mean(f.strike);
mdip=mean(f.dip);


n=[-cosd(mstrike)*sind(mdip),+sind(mstrike)*sind(mdip),-cosd(mdip)];
d=[+cosd(mstrike)*cosd(mdip),-sind(mstrike)*cosd(mdip),-sind(mdip)];
s=[-sind(mstrike),-cosd(mstrike), 0];

px=reshape((xp(:)-origx)*s(1)+(yp(:)-origy)*s(2)+zp(:)*s(3),4,N);
py=reshape((xp(:)-origx)*d(1)+(yp(:)-origy)*d(2)+zp(:)*d(3),4,N);
pz=reshape((xp(:)-origx)*n(1)+(yp(:)-origy)*n(2)+zp(:)*n(3),4,N);

peqx=(eqx04-origx)*s(1)+(eqy04-origy)*s(2)+eqdepth04*s(3);
peqy=(eqx04-origx)*d(1)+(eqy04-origy)*d(2)+eqdepth04*d(3);
peqz=(eqx04-origx)*n(1)+(eqy04-origy)*n(2)+eqdepth04*n(3);
pos=find(abs(peqz)<4 & peqx < max(px(:)) & peqx > min(px(:)) & peqy > min(py(:)) & peqy < max(py(:)));
plot(peqx(pos),peqy(pos),'ko','MarkerSize',3)

hold on
patch(px,py,zeros(4,N),up);
cmap=rot90(hot)';
    colormap(cmap)
colorbar
set(gca,'clim',[0 1])
xlabel('strike direction');
ylabel('dip dircetion');
title(sprintf('%s (cm/yr)',fault_models{i}));
axis equal tight, box on

%% print the list of new stations

zone=11;
lat0=+35.8150;
lon0=-120.370;
[x0,y0]=utm2ll(lon0,lat0,zone,1);

[lon ,lat ]=utm2ll(x0+myx(inds),y0+myy(inds),zone,2);
[lonp,latp]=utm2ll(x0+myxp(inds),y0+myyp(inds),zone,2);


fprintf('New position for %d additional GPS stations:\n',ADD);
fprintf('# longitude, latitude, longitude'', latitude''\n',ADD);
fprintf('%f %f %f %f\n',[lon,lat,lonp,latp]');