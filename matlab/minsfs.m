function [res,orig] = sbs(Gorig,Gnew,nreq,resinds,shsflag)
%%%%Performs observation subset selection using Sequential Backtracking method following Reeves and Heck (1993).
% Inputs:
% Gorig  = Original Greens function matrix for the problem d = G*m+e. (m x n and m < n)
% Gnew   = Greens function matrix corresponding to new stations.
% nreq  = Number of new stations to be added. (nreq =< m and nreq >=n)
% resinds = Indices of the resolution matrix for trace calculation.
% shsflag = Flag for allowing SHS improvement at every stage. 'Y' or 'N'.
% Outputs:
%   res.inds   = Indices of new stations from Gnew in the solution
%   res.frac   = Improvement in trace of matrix.
%   res.resmat = Resolution element values for elements indicated by resinds.
%   orig.rorig  = Original trace value.
%   orig.resmat = Resolution element values for elements indicated by resinds for original problem.

Norig=size(Gorig,1);    %Number of rows
Ncol =size(Gorig,2);    %Number of columns
Nnew = size(Gnew,1)/2;   %Since we use two rows for every new station.

Q = Gorig*Gorig';
Q = 0.5*(Q+Q');
R = Gorig'*pinv(Q)*Gorig;
Rd = diag(R);
Rdorig = Rd(resinds);  %%Original values for resolution 
rorig = min(Rdorig);

%%%%%Copying into orig output structure.
g = struct('rorig',0,'resmat',zeros(length(resinds),1),'fullres',zeros(Ncol,1));   %Dummy struct
orig = g;
orig.rorig  = rorig;
orig.resmat = Rdorig;


%%%%%Setting up res output structure.
g=struct('inds',ones(10,1),'frac',0,'resmat',zeros(length(resinds),1)); %Dummy struct
res = repmat(g,nreq,1);

%%%%Starting the iterations.
count = 1;
inds = [];
Gmat  = Gorig;

for nrow = 1:nreq,   %%%%Number of additional stations.

  indsleft = setdiff([1:Nnew],inds);  %%%All stations not selected.
  numleft = length(indsleft);
  trv = zeros(numleft,1);
  for k=1:numleft,
       G=[Gmat;Gnew(indsleft(k)*2+[-1:0],:)];
       Q=G*G';
       Q=0.5*(Q+Q');
       R=G'*pinv(Q)*G;
       Rd = diag(R);
       trv(k) = min(Rd(resinds));
  end;
  
  l=find(trv == max(trv));   %%%Find maximum.
  inds = [inds,indsleft(l)];

  if(strcmpi(shsflag,'Y') & (nrow>1))
     inds1 = minshs(Gorig,Gnew,inds,resinds); 
     inds = inds1;
  end;
  kinds = 2*repmat(inds,2,1)+repmat([-1:0]',1,length(inds));
  kinds = kinds(:);
  Gmat = [Gorig;Gnew(kinds,:)];

  %%%%Recompute stats for storing.
  Q=Gmat*Gmat';
  Q=0.5*(Q+Q');
  R=Gmat'*pinv(Q)*Gmat;
  Rd=diag(R);
  res(nrow).inds = inds;
  res(nrow).resmat = Rd(resinds);
  res(nrow).fullres = Rd;
  res(nrow).frac = min(Rd(resinds))-rorig;
end;

