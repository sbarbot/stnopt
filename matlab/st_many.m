% determine the position of P new GPS stations to optimize
% resolution in the seimogenic zone

clear all


%% Data loading
wdir='..';

% Poisson's solid
nu=1/4;

% bounds on GPS location
dxlim=[-80 60];
dylim=[-100 80];

%%%%GPS station positions.
stxlim = [-30 30]; numx = 40;
stylim = [-30 30]; numy = 40;
Nreq = 40;   %Number of additional stations required.

% fault models
fault_models={...
%    'parkfield_coseismic_2x2',...
     'pkfd_2x2',...
%    'parkfield_intermediate',...
    };

resids={
    [63:74,83:94],...
%    7:12 ...
    };

% sample segments to patches
M=0;
for i=1:length(fault_models)
    fname=[wdir '/faults/' fault_models{i} '_patch.flt'];
    [index,~,~,~,~,~,~,~,~]=...
        textread(fname,'%u %f %f %f %f %f %f %f %f','commentstyle','shell');
    M=M+size(index,1); 
end

%% build constraints and smoothing
fm=[];     % fault models
index=[];  % index of individual patches
neighbors=4; % number of neighbors for the Laplacian stencil
for i=1:length(fault_models)
    fname=[wdir '/faults/' fault_models{i} '_patch.flt'];
    [local_index,x1,x2,x3,len,width,strike,dip,rake]=...
        textread(fname,'%u %f %f %f %f %f %f %f %f','commentstyle','shell');
    flt=[x1,x2,x3,len,width,strike,dip,rake];
    N=size(flt,1);
    fm=[fm;flt];
    index=[index; i*1000+local_index];
end
M=size(fm,1);


%% GPS data and Green function
fname=[wdir '/gps/pbo_stations_km.dat'];
[staNam,gx,gy]=textread(fname,'%s %f %f','commentstyle','shell');
pos=find(gx>=dxlim(1) & gx<=dxlim(2) & gy>=dylim(1) & gy<=dylim(2));
staNam=staNam(pos);gx=gx(pos);gy=gy(pos);

sx = linspace(stxlim(1),stxlim(2),numx);
sy = linspace(stylim(1),stylim(2),numy);
[myx,myy] = meshgrid(sx,sy);
myx = myx(:); myy = myy(:);

%mysta = sprintf('M%03d',[1:length(myx)]');
%mysta = reshape(mysta,[4 length(myx)])';

% number of GPS stations
D1=length(staNam);

% GPS coordinates
yx1=[gy,gx];

% GPS displacement data
VECSIZE1=2;

% build GPS Green functions
DGF=1;
[G1,REF]=okada_gps(nu,yx1,D1,VECSIZE1,fm,DGF);
CST1=size(REF,2);
Ga = [G1,REF];

[G2,REF2]=okada_gps(nu,[myy,myx],length(myx),VECSIZE1,fm,DGF);
Gb=[G2,REF2];


[res,orig]=ressfs(Ga,Gb,Nreq,resids{1},'Y');


%% Plot results

xlim=[-80 60];
ylim=[-80 90]; % around Parkfield
cxlim=[-100 100];
cylim=[-50 100];

load ca_faults_dim.dat
load ca_faults_km.dat
load ca_coast_dim.dat
load ca_coast_km.dat

% plot data and forward model
fontsize=8;
figure(1);clf;cla
hold on
plot_faults(ca_faults_km,ca_faults_dim,xlim,ylim);
plot_faults(ca_coast_km,ca_coast_dim,cxlim,cylim);
plot(0,0,'kp','MarkerSize',10,'MarkerFaceColor','r')
plot(-12.5514, 15.8870,'kp','MarkerSize',10,'MarkerFaceColor','g')
plot(gx,gy,'k^')
plot(myx(res(end).inds),myy(res(end).inds),'ko');
%for k=1:length(staNam)
%    text(gx(k),gy(k),staNam(k),'FontSize',8)
%end
axis equal tight;box on
set(gca,'xlim',xlim,'ylim',ylim)
box on
ylabel('north (km)')
title('GPS position')


save SHS_new.mat myx myy res orig
