function [G1,REF]=okada_gps(nu,yx,D,VECSIZE,fm,DGF)

% build GPS Green functions
DGF=1;
M=size(fm,1);
G1=zeros(VECSIZE*D,DGF*M);
for k=1:M
    xg=fm(k,2);
    yg=fm(k,1);
    zg=fm(k,3);
    L=fm(k,4);
    W=fm(k,5);
    strike=fm(k,6)/180*pi;
    dip=fm(k,7)/180*pi;
    rake=fm(k,8);
    
    xd=yx(:,2)-xg;
    yd=yx(:,1)-yg;
    % strike slip
    [uxs,uys,uzs]=calc_okada(+1,xd,yd,nu,dip,zg,L,W,1,strike);
    % dip slip
    [uxd,uyd,uzd]=calc_okada(-1,xd,yd,nu,dip,zg,L,W,2,strike);

%    disp(strcat(num2str(k),':SS:',num2str([uxs,uys,uzs])))
%    disp(strcat('DS:',num2str([uxd,uyd,uzd])))
    
    % rake direction
    u=[cosd(rake)*uxs+sind(rake)*uxd;...
        cosd(rake)*uys+sind(rake)*uyd;...
        cosd(rake)*uzs+sind(rake)*uzd]';
    
    % interlace east and north forward models
    for l=1:VECSIZE
        G1(l:VECSIZE:end,k)=u(:,l);
    end
end

constantx=zeros(VECSIZE*D,1);constantx(1:VECSIZE:end)=-ones(D,1);
constanty=zeros(VECSIZE*D,1);constanty(2:VECSIZE:end)=+ones(D,1);
REF=[constantx constanty];
