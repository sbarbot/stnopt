#!/bin/sh

set -e
self=$(basename $0)
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

if [ "$#" -lt "2" ]; then
	echo $self": expected $self file.ps xmin/xmax/ymin/ymax. exiting."
	exit 1
fi

echo $self: $*
PSFILE=$1
bds=$2

WDIR=gmt

psxy -O -K -JX -R$bds -P -m \
         -W0.5p/110/110/110 \
        <<EOF >> $PSFILE
`cat $WDIR/ca_faults_km.xy`
EOF

psxy -O -K -JX -R$bds -P \
         -W0.5p/110/110/110 \
        <<EOF >> $PSFILE
2.65 -2.86
-20.5355 27.24
EOF

psxy -O -K -JX -R$bds -P -Sa0.4c -G240/10/10 -W0.6p/10/10/10 \
       <<EOF >> $PSFILE
0 0
EOF

psxy -O -K -JX -R$bds -P -Sa0.4c -G10/240/10 -W0.6p/10/10/10 \
       <<EOF >> $PSFILE
-12.5514, 15.8870
EOF

psxy -O -K -JX -R$bds -P -Sa0.4c -G10/10/240 -W0.6p/10/10/10 \
       <<EOF >> $PSFILE
-66.3175 -9.58303
EOF

#psxy -O -K -JX -R$bds -P -M -W1.6p,10/10/10 -L <<EOF >> $PSFILE
# 40.2  9.82E+01
#-28.2 -8.97E+01
# 28.2 -1.10E+02
# 96.6  7.77E+01
#EOF

pstext -O -K  -R$bds -JX -P \
        -G0/0/0 -D0/-0.1 \
        <<EOF >> $PSFILE
 42  -45  9 -52 2 CT San Andreas Flt
 26  -56  9 -50 2 CT San Juan Flt
 50  -20  9 -54 2 CT Lost Hills Flt
-40    2  9 -50 2 CT Rinconada Flt
  0  -54  9 -46 2 CT La Panza Flt
-65  -18  9 -42 2 CT San Simeon
EOF

#psxy -O -K -JX -R$bds -P -m \
#         -W0.5p/10/10/10 -St0.2c \
#        <<EOF >> $PSFILE
#`awk '{print $4,$3}' $WDIR/cont_gps_km.dat`
#EOF

#pstext -O -K  -R$bds -JX -P \
#        -G0/0/0 -D0/-0.1 \
#        <<EOF >> $PSFILE
#`awk '{print $4,$3," 12 0 4 CT ",$2}' $WDIR/cont_gps_km.dat`
#EOF

#pstext -O -K  -R$bds -JX -P \
#        -G0/0/0 -D0/-0.1 \
#        <<EOF >> $PSFILE
#`cat landers_km.flt | awk 'BEGIN{c=0;}{if ($5 == 0){c=c+1;print $4,$3," 12 0 4 CT ",c}}'`
#EOF


