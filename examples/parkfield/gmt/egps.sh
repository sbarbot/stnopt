#!/bin/sh

set -e
self=$(basename $0)
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

if [ "$#" -lt "2" ]; then
	echo $self": expected $self file.ps xmin/xmax/ymin/ymax. exiting."
	exit 1
fi

echo $self: $*
PSFILE=$1
bds=$2

WDIR=$(dirname $PSFILE)
INDEX=`echo $(basename $PSFILE) | awk '{print substr($0,0,3)}'`
DDIR=./gps

STN=$WDIR/new_stations_km.i20

psxy -O -K -JX -R$bds -P \
	-St0.3c -W0.5px,0/0/0 -G110/110/110 \
	<<EOF >> $PSFILE
`awk '{print $2,$3}' gmt/parkfield_pbo_sub.dat`
EOF

psxy -O -K -JX -R$bds -P \
        -S+0.3c -W0.5px,0/0/0 \
        <<EOF >> $PSFILE
`join -o 2.2,2.3 $STN $WDIR/new_stations_km.dat`
EOF

psxy -O -K -JX -R$bds -P \
        -Sx0.2c -W0.5px,0/0/0 \
        <<EOF >> $PSFILE
`join -o 2.2,2.3 $STN $WDIR/new_stations_km.dat | awk 'BEGIN{pi=atan2(1,0)*2;strike=-36.6;ss=sin(strike/180*pi);cs=cos(strike/180*pi);xo=2.65;yo=-2.86;}{xp=cs*($1-xo)-ss*($2-yo);yp=ss*($1-xo)+cs*($2-yo);print xo-xp*cs+yp*ss,yo+xp*ss+yp*cs}'`
EOF


