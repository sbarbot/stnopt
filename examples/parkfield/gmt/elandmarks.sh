#!/bin/sh

set -e
self=$(basename $0)
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

if [ "$#" -lt "2" ]; then
	echo $self": expected $self file.ps xmin/xmax/ymin/ymax. exiting."
	exit 1
fi

echo $self: $*
PSFILE=$1
bds=$2

WDIR=./gmt
DIR=$(dirname $1)

psxy -O -K -JX -R$bds -P -M -W0.6p,10/10/10 \
       $WDIR/highway-46-41_km.xyz >> $PSFILE

pstext -O -K -JX -R$bds -P -D0/0.2c \
	<<EOF >> $PSFILE
`awk 'function abs(x){return (x<0)?-x:x;}{if (abs($2)>9 && abs($3)>9){
	print $2,$3,"  9 0 4 CB ",$1}}' $WDIR/pbo_stations_km.dat`
EOF

psxy -O -K -JX -R$bds -P -Sc0.1c -G10/10/10 -W0.6p/10/10/10 \
       <<EOF >> $PSFILE
 6.645 -10.384
-4.961   9.583
EOF

pstext -O -K -R$bds -JX -P \
        -G0/0/0 -D0.1/0 \
        <<EOF >> $PSFILE
 6.645 -10.384  9 0 4 LT Cholame
-4.961   9.583  9 0 4 LT Parkfield
60.842 -23.946  9 0 4 LT Lost Hills
24     -15      9 0 4 CT 46
EOF

pstext -O -K -R$bds -JX -P -W255/255/255O \
        -G0/0/0 -D0.1/0 \
        <<EOF >> $PSFILE
24     -15      9 0 4 CT 46
EOF

