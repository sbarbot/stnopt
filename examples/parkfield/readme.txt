
# origin
# -120.3740 35.8150
echo -120.3740 35.8150 | proj +proj=utm +zone=11
195144.78       3968685.64

# to add 8 new stations and using only 2 components
OMP_NUM_THREADS=6 ../../build/stnopt run_2x2.input 8 2

# convert stnopt output to list of sorted indices
wdir=patch_2x2;stn=$wdir/new_stations_km.dat;res=$wdir/parkfield_pbo_sub.dat.log; grep "Stations:" ${res} | cut -d ":" -f 2 | awk -f awk/quicksort_fields.awk | awk -v p=$wdir/$(basename $stn .dat) '{file=p".i"NR;for (k=1;k<=NF;k++){print $k > file}}'

# plot the new GPS stations
map.sh -b -30/30/-30/30 -e ./gmt/efaults.sh -e ./gmt/egps.sh -e ./gmt/elandmarks.sh -E patch_2x2/new_2.ps

# table of new GPS stations and possible mirror locations
WDIR=patch_2x2;join -o 2.2,2.3 $WDIR/new_stations_km.i2 $WDIR/new_stations_km.dat | awk 'BEGIN{pi=atan2(1,0)*2;strike=-36.6;ss=sin(strike/180*pi);cs=cos(strike/180*pi);xo=2.65;yo=-2.86;}{xp=cs*($1-xo)-ss*($2-yo);yp=ss*($1-xo)+cs*($2-yo);printf("%f %f %f %f\n", $1+195144.78,$2+3968685.64,195144.78+xo-xp*cs+yp*ss,3968685.64+yo+xp*ss+yp*cs)}' | invproj +proj=utm +zone=11 -f "%f" | awk '{print $3,$4,$1,$2}' | invproj +proj=utm +zone=11 -f "%f"
