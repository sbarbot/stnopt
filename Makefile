LIBS = -L/sw/lib -lcblas -llapack -lm
CC = /sw/bin/gcc-fsf-4.9 -g -I/sw/include

### Compilation flags
CFLAGS	=	-O3  

SRC=src
WDIR=build

OBJS	= $(patsubst %,$(WDIR)/%, gpsio.o green.o stnopt.o resol.o OptimAlgo.o)
##################################################################
### THERE SHOULD BE NOTHING YOU WANT TO CHANGE BELOW THIS LINE ###
##################################################################


### The programs we want
PROGS	=	stnopt

### Compilation
default:	all
all:		$(PROGS) dummy

# dummy, since else makefile want to recompile the last util.
dummy:

# the utilities.
stnopt:		$(OBJS)
	$(CC) $(CFLAGS) $(LIBS) $(OBJS) -o $(WDIR)/$@ 

$(WDIR)/%.o:$(SRC)/%.c
	$(CC) -c $(SRC)/$*.c -o $(WDIR)/$*.o


### Helpers ###
clean:	
	@rm -f $(WDIR)/*.o $(WDIR)/stnopt

