// *********************************************************************
// Utilities for Optimal GPS network computation. 
// ---------------------------------------------------------------------
// AUTHOR: Piyush Agram
// ---------------------------------------------------------------------
// DATE: 20.12.2011
// WRITTEN: 
//
// AUTOR: Sylvain Barbot
// DATE: 8.11.2012
// WRITTEN: include an option for signal-to-noise-ratio
// *********************************************************************

#include <stdio.h>  
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <cblas.h>
#include <limits.h>

//Structure for storing data relevant to one GPS station.
typedef struct {
char name[8];    //station name
double x;         //x position in km
double y;         //y position in km
double *Gf;      //Array of Greens functions.
} GPSstn;


//Structure for storing data related to optimization problem.
typedef struct {
int Nold;           //Number of old GPS stations
int Nadd;          //Number of new stations desired
int Nnew;           //Number of new candidate stations
int Npatch;         //Number of elements in the model
int Nopt;           //Number of elements to optimize over
int Ncomp;          //Number of components considered
int *index;         //Indices of elements to optimize over
double snr;         //expected signal-to-noise ratio
char fltname[1024]; //Fault parameter file
char oldname[1024];  //Old GPS file name
char newname[1024];  //Candidate GPS station location file name
char elemname[1024]; //Element indices file name
char outname[1024];  //Output file name
char optim[16];       //Type of optimization. "MEAN" or "MAXIMIN"
} params;


//Structure related to geometry of a single fault patch.
typedef struct {
int ind;           //Index of the patch
double x[3];       //3D position in km
double len;        //Length in km
double wid;        //Width in km
double strike;     //Strike in degrees 
double dip;        //Dip in degrees
double rake;       //Rake in degrees
} fault;


