// *********************************************************************
// Utilities for Optimal GPS network computation.
// ---------------------------------------------------------------------
// AUTHOR    : Piyush Agram
// ---------------------------------------------------------------------
// DATE      : 20.12.2011
// WRITTEN    :
// *********************************************************************

#include "geoC.h"
#include <stdio.h>
#define PI 3.14159265358979323846

//Function to read the configuration file.
void ReadConfigFile(char *filename, params* inputs)
{
    
    FILE* infile;
    int nlines,nparams,nfields;
    char buf[1024],str1[1024],str2[1024];
    int MAXLEN = 1024;
    
    //Initialize to zero value
    strcpy(inputs->fltname,"");
    strcpy(inputs->oldname,"");
    strcpy(inputs->newname,"");
    strcpy(inputs->elemname,"");
    strcpy(inputs->outname,"");
    strcpy(inputs->optim,"");
    inputs->Nold = 0;
    inputs->Nnew = 0;
    inputs->Npatch = 0;
    inputs->Nopt = 0;
    inputs->index = NULL;
    inputs->snr = 1.1;
    
    infile = fopen(filename,"r");
    if(infile == NULL)
    {
        printf("Error reading configuration File - %s \n ",filename);
        exit(1);
    }
    
    nlines =0 ;
    nparams = 0 ;
    while(1)
    {
        strcpy(buf,"");
        fgets(buf,MAXLEN,infile);
        if(feof(infile) && (!strlen(buf))) break;
        nlines++;
        nfields = sscanf(buf,"%s %s",str1,str2);
        if(nfields==1 && isalnum(str1[0]))
        {
            printf("unrecognized configuration parameter %s in (%s,%d) \n",str1,filename,nlines);
            exit(1);
        }
        
        if(nfields==2 && isalnum(str1[0]))
        {
            nparams++;
            if(!strcmp(str1,"OLD_NAME"))
                strcpy(inputs->oldname,str2);
            else if (!strcmp(str1,"CAND_NAME"))
                strcpy(inputs->newname,str2);
            else if (!strcmp(str1,"FAULT_NAME"))
                strcpy(inputs->fltname,str2);
            else if (!strcmp(str1,"OUT_NAME"))
                strcpy(inputs->outname,str2);
            else if (!strcmp(str1,"ELEMENT_NAME"))
                strcpy(inputs->elemname,str2);
            else if (!strcmp(str1,"OPTIMIZATION"))
                strcpy(inputs->optim,str2);
            else if (!strcmp(str1,"SNR"))
                inputs->snr=atof(str2);
            else
            {
                printf("Unrecognizable config parameter. \n");
                printf("%s (%s, %d) \n",str1,filename,nlines);
                exit(1);
            }
            
        }
    }
    fclose(infile);
    
    //Default optimization technique
    if(strlen(inputs->optim) == 0)
        strcpy(inputs->optim,"MAXIMIN");
    
    
    //Checking if all the needed values have been initialized.
    if((strlen(inputs->fltname)==0) || (strlen(inputs->elemname)==0) || (strlen(inputs->outname)==0) || (strlen(inputs->oldname)==0) || (strlen(inputs->newname)==0))
    {
        printf("All input parameters not specified. \nCheck Again. \n");
        exit(1);
    }
}


// Read a GPS coordinate file into the structure array.
// NAME posx posy.
int loadgps(char *fname, GPSstn** gps)
{
    
    FILE *infile;
    int nlines,nparams;
    char buf[1024];
    int MAXLEN = 1024;
    //First determine number of legit stations. (comment style)
    
    infile = fopen(fname,"r");
    if(infile == NULL)
    {
        printf("Error reading old GPS File - %s \n ",fname);
        exit(1);
    }
    
    nlines = 0;
    while(1)
    {
        buf[0] = '\0';
        fgets(buf,MAXLEN,infile);
        if(feof(infile) && (!strlen(buf))) break;
        if(isalnum(buf[0]))  //Non comment line
            nlines++;
    }
    fclose(infile);
    infile = NULL;
    
    
    nparams = nlines;
    //Allocate memory for the old stations.
    *(gps) = (GPSstn*) malloc(sizeof(GPSstn)*nlines);
    
    //Read in the data from old file
    infile=fopen(fname,"r");
    nlines = 0;
    while(1)
    {
        strcpy(buf,"");;
        fgets(buf,MAXLEN,infile);
        if(feof(infile) && (!strlen(buf))) break;
        
        if(isalnum(buf[0])) //Legit line
        {
            sscanf(buf,"%4s %lf %lf",&((*gps)[nlines].name),&((*gps)[nlines].x),&((*gps)[nlines].y));
            nlines++;
        }
    }
    
    fclose(infile);
    if(nlines != nparams)
    {
        printf("Inconsistency in old file input. Exiting .... \n");
        exit(1);
    }
    return nlines;
}

//Load the fault patch files.
//Index x y z len wid strike dip rake
void loadflt(params* inputs, fault** patch)
{
    
    FILE *infile;
    int nlines,nparams;
    char buf[1024];
    int MAXLEN = 1024;
    
    //First determine number of legit stations. (comment style)
    
    infile = fopen(inputs->fltname,"r");
    if(infile == NULL)
    {
        printf("Error reading fault File - %s \n ",inputs->fltname);
        exit(1);
    }
    
    nlines = 0;
    while(1)
    {
        buf[0] = '\0';
        fgets(buf,MAXLEN,infile);
        if(feof(infile) && (!strlen(buf))) break;
        if(isalnum(buf[0]))  //Non comment line
            nlines++;
    }
    fclose(infile);
    infile = NULL;
    
    inputs->Npatch = nlines; //Set number of old stations.
    
    //Allocate memory for the old stations.
    *(patch) = (fault*) malloc(sizeof(fault)*nlines);
    
    //Read in the data from old file
    infile=fopen(inputs->fltname,"r");
    nlines = 0;
    while(1)
    {
        strcpy(buf,"");
        fgets(buf,MAXLEN,infile);
        if(feof(infile) && (!strlen(buf))) break;
        
        if(isalnum(buf[0])) //Legit line
        {
            sscanf(buf,"%d %lf %lf %lf %lf %lf %lf %lf %lf",&((*patch)[nlines].ind),&((*patch)[nlines].x[0]),&((*patch)[nlines].x[1]),&((*patch)[nlines].x[2]),&((*patch)[nlines].len),&((*patch)[nlines].wid),&((*patch)[nlines].strike),&((*patch)[nlines].dip),&((*patch)[nlines].rake));
            (*patch)[nlines].strike = (*patch)[nlines].strike * PI/180.0;    //Convert to radian immediately.
            (*patch)[nlines].dip = (*patch)[nlines].dip * PI/180.0;
            (*patch)[nlines].rake = (*patch)[nlines].rake * PI/180.0;
            nlines++;
        }
    }
    
    fclose(infile);
    if(nlines != inputs->Npatch)
    {
        printf("Inconsistency in flt file input. Exiting .... \n");
        exit(1);
    }
}


//Load the Element indices to optimize over.
//One integer per line
void loadelem(params* inputs)
{
    
    FILE *infile;
    int nlines,nparams;
    char buf[1024];
    int MAXLEN = 1024;
    //First determine number of legit stations. (comment style)
    
    infile = fopen(inputs->elemname,"r");
    if(infile == NULL)
    {
        printf("Error reading old GPS File - %s \n ",inputs->oldname);
        exit(1);
    }
    
    nlines = 0;
    while(1)
    {
        buf[0] = '\0';
        fgets(buf,MAXLEN,infile);
        if(feof(infile) && (!strlen(buf))) break;
        if(isalnum(buf[0]))  //Non comment line
            nlines++;
    }
    fclose(infile);
    infile = NULL;
    
    inputs->Nopt = nlines; //Set number of old stations.
    
    //Allocate memory for the old stations.
    inputs->index = (int*) malloc(sizeof(int)*nlines);
    
    //Read in the data from old file
    infile=fopen(inputs->elemname,"r");
    nlines = 0;
    while(1)
    {
        strcpy(buf,"");
        fgets(buf,MAXLEN,infile);
        if(feof(infile) && (!strlen(buf))) break;
        
        if(isalnum(buf[0])) //Legit line
        {
            sscanf(buf,"%d ",&(inputs->index[nlines]));
            nlines++;
        }
    }
    
    fclose(infile);
    if(nlines != inputs->Nopt)
    {
        printf("Inconsistency in element file input. Exiting .... \n");
        exit(1);
    } 
}

