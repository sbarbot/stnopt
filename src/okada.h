#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI 3.14159265358979323846
#define NU 0.25
#define EPSN 1.0e-15

//Adapted from Sylvains code.
void fBi(double sig, double eta, double a, double delta, double p, double q, double *f,int flag)
{

//Code derived exactly from fBi.m
//Matlab code provided by Sylvain Barbot.
//Look at Matlab code for documentation.
//Flag = 1 for strike slip, Flag = 2 for Dip Slip, Flag = 3 for Tensile.
   double cosd,sind,tand;
   double cosd2,sind2,cssnd;
   double R,X,ytil,dtil;
   double Rdtil,Rsig,Reta,RX;
   double lnRdtil,lnReta,lnReta0;
   double ORRsig,OReta,ORReta;
   double I1,I2,I3,I4,I5,theta;
    
   cosd = cos(delta);
   sind = sin(delta);
   tand = sind/cosd;
   cosd2 = cosd*cosd;
   sind2 = sind*sind;
   cssnd = cosd*sind;

   R = sqrt(sig*sig+eta*eta+q*q);
   X = sqrt(sig*sig+q*q);
   ytil = eta*cosd + q*sind;
   dtil = eta*sind - q*cosd;


   Rdtil = R + dtil;
   Rsig = R + sig;
   Reta = R + eta;
   RX = R + X;
   
   lnRdtil = log(Rdtil);
   lnReta  = log(Reta);
   lnReta0 = -log(R-eta);
   ORRsig = 1.0/(R*Rsig);
   OReta = 1.0/Reta;
   ORReta = 1.0/(R*Reta);

   if(fabs(Reta) < EPSN)
   {
      lnReta = lnReta0;
      OReta = 0.0;
      ORReta = 0.0;
   }

   if(fabs(Rsig) < EPSN)
   {
      ORRsig = 0.0;
   }
  
   if(fabs(q) < EPSN)
      theta = 0.0;
   else 
      theta = atan((sig*eta)/(q*R));
   
 
//   printf("Theta: %lf %lf %lf %lf %lf \n",theta, sig, eta, q , R);  
   if(fabs(cosd) < EPSN)
   {
       I5 = -a * sig * sind / Rdtil;
       I4 = -a * q / Rdtil;
       I3 = 0.5*a*((eta/Rdtil)+(ytil*q)/(Rdtil*Rdtil)-lnReta);
       I2 = -a*lnReta-I3;
       I1 = -0.5*a*sig*q/(Rdtil*Rdtil);
   }
   else
   {
       if(fabs(sig) < EPSN)
            I5 = 0;
       else
            I5 = 2.0*a*atan((eta*(X+q*cosd) +X*RX*sind)/(sig*RX*cosd))/cosd; //check matlab
       I4 = a *(lnRdtil - sind * lnReta)/cosd;                               //check matlab
       I3 = a * (ytil/(cosd*Rdtil) - lnReta) + tand*I4;                      //check matlab
       I2 = -a*lnReta-I3;
       I1 = -a*sig/(cosd*Rdtil) - tand*I5;				     //check matlab
   }

/*       printf("Is: %lf %lf %lf %lf %lf \n",I1,I2,I3,I4,I5);
       printf("ORs: %lf %lf %lf \n",OReta,ORReta,ORRsig);
       printf("lns: %lf %lf %lf \n",lnRdtil,lnReta,lnReta0);
       printf("As: %lf %lf %lf %lf \n",sig,q,ytil,dtil);
       printf("Ts: %lf %lf %lf %lf \n",sind,cosd,cssnd,theta);
*/
   if(flag == 1)      //Strike Slip Equations
   {
       f[0] = (sig*q)*ORReta+theta+I1*sind;
       f[1] = (ytil*q)*ORReta +(q*cosd)*OReta+I2*sind;
       f[2] = (dtil*q)*ORReta + (q*sind)*OReta+I4*sind;
   }
   else if(flag == 2)  //Dip Slip Equations
   {
        f[0] = (q/R) - I3*cssnd;
        f[1] = (ytil*q)*ORRsig + cosd*theta - I1*cssnd;
        f[2] = (dtil*q)*ORRsig + sind*theta - I5*cssnd;
   }
   else if(flag == 3)  //Tensile Equations
   {
        f[0] = q*q*ORReta - I3*sind2;
        f[1] = (-dtil*q)*ORRsig - sind*(sig*q*ORReta - theta) -I1 * sind2;
        f[2] = (ytil*q)*ORRsig+cosd*((sig*q)*ORReta - theta) - I5*sind2;
   }
}
   

//Unit strike slip.
void calc_okada(double x,double y,double dip,double* xyz,double L,double W,double strike,double *u,int flag)
{
//Computes the Green's function at one GPS station due to one patch (Unit slip assumed).
//x      => X coordinate of the GPS station
//y      => Y coordiante of the GPS station
//dip    => Dip in radians of the fault patch
//xyz    => double[3] array of the patch location
//L      => Length of the patch
//W      => Width of the patch
//strike => Strike in radians of the fault patch
//u      => double[3] output
//flag   => 1. Strike slip. 2. Dip Slip. 3. Tensile.
   double cosd, sind, tand;
   double coss, sins, d;
   double rotx,roty;
   double a,p,q,Cons;
   double fa[3],fb[3],fc[3],fd[3];
   double uxj,uyj;
   
   x = x - xyz[1];     //w.r.t to the patch
   y = y - xyz[0];     //w.r.t to the patch
   cosd = cos(dip);
   sind = sin(dip);
   coss = cos(strike);
   sins = sin(strike);

   d = xyz[2] + W*sind;
   x = x - W*cosd*coss - sins*L/2.0;
   y = y + W*cosd*sins - coss*L/2.0;

   strike = -strike+ (PI/2.0);
   coss = cos(strike);
   sins = sin(strike);
   rotx = x*coss+y*sins;
   roty = -x*sins+y*coss;
  
   L = L/2.0;
   Cons= -0.5/PI;
   p = roty*cosd + d*sind;
   q = roty*sind - d*cosd;
   a = 1-2*NU;

   //Computing at the 4 corners.
   fBi(rotx+L,p,a,dip,p,q,fa,flag);
   fBi(rotx+L,p-W,a,dip,p,q,fb,flag);
   fBi(rotx-L,p,a,dip,p,q,fc,flag);
   fBi(rotx-L,p-W,a,dip,p,q,fd,flag);

   uxj = Cons*(fa[0]-fb[0]-fc[0]+fd[0]);
   uyj = Cons*(fa[1]-fb[1]-fc[1]+fd[1]);
   u[2] = Cons*(fa[2]-fb[2]-fc[2]+fd[2]);
   u[0] = -uyj*sins + uxj*coss;
   u[1] = uxj*sins + uyj*coss;
}


