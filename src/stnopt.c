// *********************************************************************
// Wrapper for Optimal GPS network computation.
// ---------------------------------------------------------------------
// AUTHOR: Piyush Agram
//
// ---------------------------------------------------------------------
// DATE: 20.12.2011
// WRITTEN:
// AUTHOR: Sylvain Barbot
// DATA: 8.11.2012
// WRITEN: include signal-to-noise ratio in the resolution calculation
// *********************************************************************

#include "geoC.h" 

//The main program
int main(int argc, char *argv[])
{
    
    params input;      //For storing optimization problem details.
    GPSstn *oldgps;    //For storing already existent network.
    GPSstn *newgps;    //For storing candidate station information.
    fault  *patch;     //For storing the fault geometries.
    int i,j,k;         //Index variables
    double *G;         //For Greens functions
    double *Q;         //For resolution
    double *work1;     //For matrix calculations. Preallocated to save time.
    double *work2;
    double ores;       //For original objective function value.
    double mres;       //For new objective function values.
    char logfile[512];     //Output log file with all details.
    FILE *fout;            //Output file handle.
    int *addidx;           //Index of stations already added.
    int f;
    
    if(argc !=4)
    {
        printf("Usage: stnopt config_file numadd ncomp\n\n");
        printf("Arg 1) Config file -> simple text file that contains at least \n");
        printf("      OLD_NAME       old-gps-station-locs \n");
        printf("     CAND_NAME       new-gps-station-locs \n");
        printf("    FAULT_NAME       Fault-patch-file \n");
        printf("      OUT_NAME       Optimization-output-name \n");
        printf("  ELEMENT_NAME     Indices-of-patches-for optimization \n");
        printf("Arg 2) numadd -> Number of stations to add. \n");
        printf("Arg 3) ncomp  -> Number of components (2 or 3). \n");
        
        exit(0);
    }
    
    input.Nadd = atoi(argv[2]);
    input.Ncomp = atoi(argv[3]);
    
    
    if((input.Ncomp != 2) && (input.Ncomp != 3))
    {
        printf("Invalid Number of Components: %d \n",input.Ncomp);
        printf("Code supports 2 (or) 3 components  \n");
        exit(1);
    }
    
    //Parsing the config file
    ReadConfigFile(argv[1],&input);
    printf("Reading the Input Configuration File - %s \n",argv[1]);
    printf("***************************************\n");
    printf("OLD-GPS   => %s \n",input.oldname);
    printf("NEW-GPS   => %s \n",input.newname);
    printf("FLT-NAME  => %s \n",input.fltname);
    printf("ELEM-NAME => %s \n",input.elemname);
    printf("SNR       => %f \n",input.snr);
    printf("Number of Components => %d \n",input.Ncomp);
    printf("Number of new STNs   => %d \n",input.Nadd);
    printf("***************************************\n");
    
    //Parsing the OLD GPS file
    input.Nold = loadgps(input.oldname,&oldgps);
    printf("\n \n OLD GPS STATION INFO - %d stations \n",input.Nold);
     printf("******************************************\n");
     /* for(i=0;i<(input.Nold);i++)
     printf("%6s  %5.4lf  %5.4lf \n",oldgps[i].name,oldgps[i].x,oldgps[i].y);
     */
    //Parsing the Fault parameter file.
    loadflt(&input,&patch);
     printf("\n \n PATCH FILE INFO - %d patches \n",input.Npatch);
     printf("********************************************\n");
     /* for(i=0;i<10;i++)
     printf("%d %5.4lf %5.4lf %5.4lf \n",patch[i].ind,patch[i].x[0],patch[i].x[1],patch[i].x[2]);
      */
    
    //Setting up greens functions for each old station.
    for(i=0;i<input.Nold;i++)
    {
        setup_green(patch,&(oldgps[i]),input.Npatch,input.Ncomp,'n');
        
        //    For checking green's functions.
        /*      if(i==1)
         {
         for(j=0;j<input.Npatch;j++)
         printf("%d %lf %lf \n",j+1,oldgps[i].Gf[j],oldgps[i].Gf[j+input.Npatch+input.Ncomp]);
         }
         */
    }
    printf("\n*********************************************\n");
    printf("Number of OLD GPS Stations: %d \n",input.Nold);
    //Loading candidate GPS locations.
    input.Nnew = loadgps(input.newname,&newgps,input.Ncomp);
    
    //Setting up greens functions for candidate stations.
    for(i=0;i<input.Nnew;i++)
    {
        setup_green(patch,&(newgps[i]),input.Npatch,input.Ncomp);
    }
    
    printf("Number of NEW GPS Stations: %d \n",input.Nnew);
    
    //Reading up patch indices for optimization
    loadelem(&input);
    //for(i=0;i<input.Nopt;i++)
          //printf("Index: %d %d\n",i+1,input.index[i]);
    
    
    printf("Number of optimization elements: %d \n",input.Nopt);
    printf("************************************************\n");
    
    
    //Allocating memory for the optimization problem.
    j = (input.Npatch+input.Ncomp); // j= no of patches + no of components
    k = (input.Ncomp)*(input.Nold+input.Nadd); // k = No of components * (No of old + new gps stations)
    i = (j>k)? j:k;
    G = (double*) malloc(sizeof(double)*i*i);
    Q = (double*) malloc(sizeof(double)*i*i);
    if((G==NULL) || (Q==NULL))
    {
        printf("Not enough memory for matrix computations. \n");
        exit(1);
    }
    
    work1 = (double*) malloc(sizeof(double)*i*i);
    work2 = (double*) malloc(sizeof(double)*i*i);
    if((work1 == NULL) || (work2 == NULL))
    {
        printf("Not enough memory for matrix work computations. \n");
        exit(1);
    }
    
    //Set up output logfile.
    strcpy(logfile,input.oldname);
    strcat(logfile,".log");
    fout = fopen(logfile,"w");
    
    //Set up common part of G matrix
    basicG(oldgps,&input,G);
    
    //Can remove redundant data for the old GPS network to save memory.
    //Set up common part of G matrix
    for(i=0;i<input.Nold;i++)
        free((char*)(oldgps[i].Gf));
    free((char*) oldgps);
    
    //Set up common part of G matrix
    //Compute original resolution
    j = input.Ncomp+input.Npatch;
    i = input.Nold*input.Ncomp;
    if(input.Nold>0)
        Resolution(G,i,j,Q,work1,work2,input.snr);
    
    printf("\n\nOriginal Objective is: \n");
    printf("************************\n");
   
    min_res(Q,input.Npatch,input.index,input.Nopt,&ores);
    printf("Value: %lf \n",ores);
    
    fprintf(fout,"Original Objective first: %lf \n",ores);
    
    
    //To store the new stations.
    addidx = (int*) malloc(sizeof(int)*input.Nadd);
    
    //Add as many stations as input.Nadd.
    for(i=0;i<input.Nadd;i++)
    {
        
        refSHS(G,input.Nold,input.Npatch,input.Ncomp,newgps,input.Nnew,addidx,
               i,input.index,input.Nopt,ores,Q, work1,work2,&mres,input.snr);
        
        printf("Add %d : New Obj Value: %lf  \n",i+1,mres);
        fprintf(fout," Add %d \n New Obj Value: %lf  \n",i+1,mres);
        printf("Stations: ");
        fprintf(fout," Stations: ");
        for(j=0;j<i+1;j++)
        {
            printf("%d\t",addidx[j]+1);
            fprintf(fout,"%d\t",addidx[j]+1);
        }
        fprintf(fout,"\n");
        printf("\n");
        
        ores = mres;
    }
    
    //Freeing all memory
    fclose(fout);
    free((char*)addidx);
    free((char*)G);
    free((char*)Q);
    free((char*)work1);
    free((char*)work2);
    
    for(i=0;i<input.Nnew;i++)
        free((char*)(newgps[i].Gf));
    
    free((char*) patch);
    return 0;
}
