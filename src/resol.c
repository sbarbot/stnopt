// *********************************************************************
// Utilities for Optimal GPS network computation.
// ---------------------------------------------------------------------
// AUTHOR: Piyush Agram
// ---------------------------------------------------------------------
// DATE: 20.12.2011
// WRITTEN:
// AUTHOR: Sylvain Barbot
// DATA: 8.11.2012
// WRITTEN: add signal-to-noise ratio in the resolution calculation
// *********************************************************************

#include "geoC.h"
#define RMIN 1.11e-16

//Headers for LAPACK routines

//Eigen values of a symmetric double precision array.
void dsyev_(const char* JOBZ, const char* UPLO, const int* N, double* A, const int* LDA, double* W, double* WORK, const int* LWORK, int* INFO);


//Compute the resolution matrix.
void Resolution(double *G, int M, int N,double *Q,double *w1,double *w2,double snr)
{
    
    //printf("\n snr in resol:%f",snr);
    
    
    //G  => Greens functions. Row major format.
    //M  => Number of rows in G.
    //N  => Number of columns in G.
    //Q  => Resolution matrix of G.
    //work => Workspace arrays.
    
    int i,j,dim;
    double temp,cond,w,cenergy,tenergy;
    char flag1,flag2;
    //FILE *foutQ;
    //foutQ = fopen("logfileQ.dat","w+");
    
    //w1 = G*G';
    cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasTrans,M,M,N,1.0,G,N,G,N,0.0,w1,M);
    
    //w1 = 0.5*w1;
    dim = M*M;
    cblas_dscal(dim,0.5,w1,1);
    
    //w1 = (w1+w1'); To Ensure symmetricity.
    for(i=0;i<M;i++)
        for(j=i;j<M;j++)
        {
            temp = w1[i*M+j]+w1[j*M+i];
            w1[i*M+j] = temp;
            w1[j*M+i] = temp;
        }
    
    //Computation of pseudo-inverse.
    flag1 = 'V';
    flag2 = 'L';
    dim = 3*M;
    dsyev_(&flag1,&flag2,&M,w1,&M,w2,Q,&dim,&j);
    if(j!=0)
    {
        printf("Symmetric Eigen value decomposition failed: %d \n",j);
        exit(1);
    }
    
    //Creating diagonal matrix and inverting. same as pinv in matlab.
    dim = M*M;
    for(i=0;i<dim;i++)
        Q[i] = 0.0;
    
    //printf("\n printing eigen values during calculation\n");
    for(i=0;i<M;i++)
    {
        Q[i*M+i] = (fabs(w2[i]) > (snr*snr))? (1.0/w2[i]):0.0;
        //printf("\n i=%d, w2[%d]= %f snr=%f Q[%d]=%f",i,i,w2[i],snr,i*M+i,Q[i*M+i]);
    }
    
    //     for(i=0;i<M;i++)
    //          printf("%d: %lf \n",i+1,w1[(M-1)*M+i]*1000);
    
    cblas_dgemm(CblasRowMajor,CblasTrans,CblasNoTrans,M,M,M,1.0,w1,M,Q,M,0.0,w2,M);
    cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,M,M,M,1.0,w2,M,w1,M,0.0,Q,M);
    
    //Q = 0.5*(Q+Q');
    cblas_dscal(dim,0.5,Q,1);
    for(i=0;i<M;i++)
        for(j=i;j<M;j++)
        {
            temp = Q[i*M+j]+Q[j*M+i];
            Q[i*M+j] = temp;
            Q[j*M+i] = temp;
        }
    
    //Q = G'*Q * G
    cblas_dgemm(CblasRowMajor,CblasTrans,CblasNoTrans,N,M,M,1.0,G,N,Q,M,0.0,w1,M);
    cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,N,N,M,1.0,w1,M,G,N,0.0,w2,N);
    
    //Picking out the diagonal.
    
    for(i=0;i<N;i++)
    {
        Q[i] = w2[i*N+i];
        //fprintf("\n Q[%d]=%f \n",i,Q[i]);
        // fprintf(foutQ,"%f\n",Q[i]);
    }
    
}


//Build the common part of the Greens function matrix with all the old stations.
void basicG(GPSstn *old,params *input,double *G)
{
    //old     -> Gps structure for the old stations.
    //input   -> struct with problem dimensions.
    //G       -> Actual matrix (Assumed to have been created with enough memory).
    int i,j,k;
    int M,N;
    int ind;
    
    M = (input->Nold);
    N = (input->Npatch + input->Ncomp)*(input->Ncomp);
    
    for(i=0; i<M;i++)
    {
        for(j=0;j<N;j++)
        {
            ind = i*N+j;
            G[ind] =  old[i].Gf[j];
        }
    }
}


