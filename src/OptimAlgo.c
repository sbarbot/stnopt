// *********************************************************************
// Actual SFS / SHS Algorithms for network augmentation.
// ---------------------------------------------------------------------
// AUTHOR    : Piyush Agram
// ---------------------------------------------------------------------
// DATE      : 20.12.2011
// WRITTEN    :
// *********************************************************************

#include "geoC.h"

void avg_res(double *res, int N, int* ind, int M, double *fnval)
{
    //res => Resolution array
    //N   => Number of elements in resolution array
    //ind => Array of optimization indices
    //M   => Size of ind
    int i;
    double sum = 0.0;
    for(i=0;i<M;i++)
        sum += res[ind[i]-1];
    sum = sum/(1.0*M);
    *fnval = sum;
}

void min_res(double *res,int N,int* ind,int M, double *fnval)
{
    //For description see avg_res
    int i;
    double minv=1000.0;
    double temp;
    for(i=0;i<M;i++)
    {
        temp = res[ind[i]-1];
        //printf("\n temp:%f",temp);
        minv = (temp< minv)? temp:minv;
        //printf("\n minv:%f",minv);
    }
    *fnval =  minv;
    //exit(0);
}


void addrowtoG(double *G,GPSstn *nstn,int rowind,int cols)
{
    //copies the Greens functions array to the end of the matrix.
    //Note that rowind is the count from zero and not one.
    int i;
    for(i=0;i<cols;i++)
    {
        G[rowind*cols+i] = nstn->Gf[i];
        //       printf("%lf \n",G[rowind*cols+i]);
    }
}


//Sequential Forward Selection (SFS)
void refSFS(double *G,int Nold,int Npatch,int Ncomp, GPSstn* newgps,int Nnew, int *index,int Nindex,int *elems,int Nelems,double ores, double *Q, double *w1,double *w2,double* fval,double snr)
{
    //basicG => Gmatrix that wont be touched.
    //newgps => The new GPS station structure
    //index => Index of stations already added.
    //Nindex => Number already added.
    //elems => Elements to optimize over
    //Nelems => Length of elems.
    //ores => Current best objective.
    
    int i,j,k;
    int Nrow,Ncol,Nlrow;
    double mres = -1000.0;       //New objective value
    int mind = -1;                 //Station index for optimum.
    double tres;
    
    Nrow = Nold * Ncomp;
    Ncol = Ncomp + Npatch;
    Nlrow = Ncomp*(Ncomp+Npatch);
    
    //Insert new stations rows which are already in solution.
    for(i=0; i< Nindex;i++)
        addrowtoG(G,&(newgps[index[i]]),Nold+i,Nlrow);
    
    Nrow = Nold*Ncomp + Nindex*Ncomp;
    
    //Checking input resolution.
    //   Resolution(G,Nrow,Ncol,Q,w1,w2);
    //   min_res(Q,Npatch,elems,Nelems,&tres);
    //   printf("Input Resol = %lf \n",tres);
    
    Nrow = Nold*Ncomp + (Nindex+1)*Ncomp;
    
    for(i=0;i<Nnew;i++) //Add one row at a time to the end.
    {
        addrowtoG(G,&(newgps[i]),Nold+Nindex,Nlrow);
        Resolution(G,Nrow,Ncol,Q,w1,w2,snr);
        
        min_res(Q,Npatch,elems,Nelems,&tres);
        
        if (tres > mres)
        {
            mres = tres;
            mind = i;
        }
    }
    
    if (mres > ores)
    {
        index[Nindex] = mind;
        *fval = mres;
    }
    else
    {
        printf("Numerical Warning. Old vs New = %lf vs %lf \n",ores,mres);
        exit(0);
    }
}


//Sequential Hybrid Selection
//Improved selection algorithm. Slower than SFS but faster.
void refSHS(double *G,int Nold,int Npatch,int Ncomp, GPSstn* newgps,int Nnew, int *index,int Nindex,int *elems,int Nelems,double ores, double *Q, double *w1,double *w2,double* fval,double snr)
{
    //basicG => Gmatrix that wont be touched.
    //newgps => The new GPS station structure
    //index => Index of stations already added.
    //Nindex => Number already added.
    //elems => Elements to optimize over
    //Nelems => Length of elems.
    //ores => Current best objective.
    
    int i,j,k;
    int Nrow,Ncol,Nlrow;
    double mres = -1000.0;       //New objective value
    int mind = -1;               //Station index for optimum.
    double tres;
    int indi,indj;
    int flag;
    
    Nrow = Nold * Ncomp;
    Ncol = Ncomp + Npatch;
    Nlrow = Ncomp*(Ncomp+Npatch);
    
    //Insert new stations rows which are already in solution.
    for(i=0; i< Nindex;i++)
        addrowtoG(G,&(newgps[index[i]]),Nold+i,Nlrow);
    
    Nrow = Nold*Ncomp + Nindex*Ncomp;
    
    //Checking input resolution.
    //   Resolution(G,Nrow,Ncol,Q,w1,w2);
    //   min_res(Q,Npatch,elems,Nelems,&tres);
    //   printf("Input Resol = %lf \n",tres);
    
    Nrow = Nold*Ncomp + (Nindex+1)*Ncomp;
    
    
    for(i=0;i<Nnew;i++) //Add one row at a time to the end. Add each of the new GPS candidate points one at a time and calculate the resolution and min res.
    {
        
        addrowtoG(G,&(newgps[i]),Nold+Nindex,Nlrow);
        Resolution(G,Nrow,Ncol,Q,w1,w2,snr);
        
        min_res(Q,Npatch,elems,Nelems,&tres); //finding min in each res
        if (tres > mres) //finding max of the min res BUT its the FIRST MAX VALUE FOUND!
        {
            
            mres = tres;
            mind = i;
        }
    }
    
    if (mres > ores) //if final min res is greater than original res, then add station
    {
        index[Nindex] = mind;
        Nindex++;
    }
    else
    {
        printf("Numerical Error. Old vs New = %lf vs %lf \n",ores,mres);
        exit(0);
    }
    
    printf("SFS Objective Value: %7lf and index is %d \n",mres, mind);
    printf("Stations: ");
    
    
    if(Nindex>1)     //To ensure we have more than one solution to swap out.
    {
        //Recreate G matrix for new optimized solution.
        for(i=0; i< Nindex;i++)
            addrowtoG(G,&(newgps[index[i]]),Nold+i,Nlrow);
        
        //So far traditional SFS. Now we look for optimal swaps.
        flag = 1;
        while(flag)
        {
            indi = -1;
            indj = -1;
            printf("SHS Iteration Number: %d , %d \n",Nindex,flag);
            for(i=0;i<Nindex;i++)    //For each of the old
            {
                for(j=0;j<Nnew;j++)
                {
                    if(j != index[i])
                    {
                        addrowtoG(G,&(newgps[j]),Nold+i,Nlrow);
                        Resolution(G,Nrow,Ncol,Q,w1,w2,snr);
                        min_res(Q,Npatch,elems,Nelems,&tres);
                        if(tres > mres)
                        {
                            mres = tres;
                            indi = i;
                            indj = j;
                        }
                    }
                }
                addrowtoG(G,&(newgps[index[i]]),Nold+i,Nlrow);      //Replace the row back to original.
            }
            
            if (indj>=0)    //Better solution has been found.
            {
                flag++;
                printf("Replacing %d by %d \n",index[indi],indj);
                index[indi] = indj;
            }
            else
                flag=0;
        } 
    } 
    *fval = mres;
    
    
}

