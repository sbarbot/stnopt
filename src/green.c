// *********************************************************************
// Utilities for Optimal GPS network computation. 
// ---------------------------------------------------------------------
// AUTHOR    : Piyush Agram
// ---------------------------------------------------------------------
// DATE      : 20.12.2011
// WRITTEN    : 
// *********************************************************************


#include "geoC.h"
#include "okada.h"

//Populates the Greens function matrix at station stn.
//The patch information is provided in the array patch.
//Npatch specifies the number of patches.
void setup_green(fault *patch,GPSstn *stn,int Npatch,int Ncomp)
{
  int i,j,k;
  double uss[3],uds[3],totalu[3];
  double cosr,sinr;

  //Allocating memory for Greens functions.
  stn->Gf = (double*) malloc(sizeof(double)*Ncomp*(Npatch+Ncomp)); //2 for frame errors.
  if(stn->Gf == NULL)
  {
     printf("Not enough memory to allocate for greens functions. \n");
     exit(1);
  }
 
  for(i=0;i<Npatch;i++)
  {
       cosr = cos(patch[i].rake);
       sinr = sin(patch[i].rake);

       //Strike Slip
       calc_okada(stn->x,stn->y,patch[i].dip,patch[i].x,patch[i].len,patch[i].wid,patch[i].strike,uss,1);
      
       //Dip Slip
       calc_okada(stn->x,stn->y,patch[i].dip,patch[i].x,patch[i].len,patch[i].wid,patch[i].strike,uds,2);

//      printf("%i:SS: %lf %lf %lf \n",i+1,uss[0]*1000,uss[1]*1000,uss[2]*1000);
//     printf("DS: %lf %lf %lf \n",uds[0]*1000,uds[1]*1000,uds[2]*1000);        
       
       //Combining using rake
       totalu[0] = cosr*uss[0]-sinr*uds[0];
       totalu[1] = cosr*uss[1]-sinr*uds[1];
       totalu[2] = cosr*uss[2]-sinr*uds[2];
       
       //Populating Gf matrix for stn.
       for(k=0;k<Ncomp;k++)
           stn->Gf[i+k*(Npatch+Ncomp)] = totalu[k];
   }
   //Frame parameters.
   for(i=0;i<Ncomp;i++)
      for(j=0;j<Ncomp;j++)
      {
          if(i == j)
             cosr = 1.0;
          else
             cosr = 0.0;

          stn->Gf[Npatch+j+i*(Npatch+Ncomp)] = cosr;
      }
}        
